
/*
psql instantation:
create table darpaRaw_25PercSample(
		sourceip TEXT,
		destip TEXT,
		day TEXT
		);
create table darpa_25PercSample(
		sourceip TEXT,
		destip TEXT,
		day date,
		numVisits int
		);

create table darpaRaw_FullSample(
		sourceip TEXT,
		destip TEXT,
		day TEXT
		);
create table darpa_FullSample(
		sourceip TEXT,
		destip TEXT,
		day date,
		numVisits int
		);


	insert into darpa_25PercSample
   select  sourceip, destip, date_trunc('day', to_date( day, 'MM/DD/YYYY-HH24:MI')) as day, count(*) as numVisits 
   from darpaRaw_25PercSample 	 group by sourceip, destip, day;

	insert into darpa_FullSample
   select  sourceip, destip, date_trunc('day', to_date( day, 'MM/DD/YYYY-HH24:MI')) as day, count(*) as numVisits 
   from darpaRaw_FullSample 	 group by sourceip, destip, day;


Code Sections:
Pre: define the darpaRaw table above (the very raw data is imported to darpa_with_labels_tenPercSample )
A. table declarations
B. function declarations
C. algorithm declaration
D. algorithm call


export PGPORT=15718   # sets the PORT to be used by PostgreSQL
export PGHOST=/tmp    # sets the directory for the socket files
initdb $HOME/826prj    # initializes a database structure on the folder $HOME/826prj
pg_ctl -D $HOME/826prj -o '-k /tmp' start # starts the server on the port YYYYY, using $HOME/826prj as data folder
createdb $USER        # creates a database, with your andrew id as its name


ssh cperez1@ghc62.ghc.andrew.cmu.edu
scp psql_imp_MarkingR.sql cperez1@ghc62.ghc.andrew.cmu.edu:~/psql_imp_markingR.sql

scp darpa_25PercSample.csv cperez1@ghc62.ghc.andrew.cmu.edu:~/darpa_25PercSample.csv

\copy darpaRaw_25PercSample FROM 'darpa_25PercSample.csv' DELIMITER ',' CSV;
\copy darpaRaw_FullSample FROM 'darpa.csv' DELIMITER ',' CSV;


scp darpa.csv cperez1@ghc62.ghc.andrew.cmu.edu:~/darpa.csv


*/


-----------------
---- A. Declare Intermediate tables
-----------------

create table currentR_log (
		rowCount int
);

-- create temporary tables containing both the current relation and the current unique values of the tensor variables
drop table currentR;
create table currentR (
		sourceip varchar(50),
		destip varchar(50),
		day varchar(50),
		numVisits int,
		markedTuple int -- 1/0 for whether it's marked
);


create table R_orig (
		sourceip varchar(50),
		destip varchar(50),
		day varchar(50),
		numVisits int
)

create table currentR2 (
		sourceip varchar(50),
		destip varchar(50),
		day varchar(50),
		numVisits int
)
create table currentSingleBlock (
		sourceip varchar(50),
		destip varchar(50),
		day varchar(50),
		numVisits int,
		blockIndex int
)
create table currentSourceUniques (
		sourceip varchar(50)
)
create table currentDestUniques (
		destip varchar(50)
)
create table currentDayUniques (
		day varchar(50)
)

-- inner loop data tables:
create table B_ori (
		sourceip varchar(50),
		destip varchar(50),
		day varchar(50),
		numVisits int
)
create table results (
		sourceip varchar(50),
		destip varchar(50),
		day varchar(50),
		numVisits int,
		blockIndex int
)
create table results_temp (
		sourceip varchar(50),
		destip varchar(50),
		day varchar(50),
		numVisits int,
		blockIndex int
)

create table inner_B (
		sourceip varchar(50),
		destip varchar(50),
		day varchar(50),
		numVisits int
)
create table inner_B_temp (
		sourceip varchar(50),
		destip varchar(50),
		day varchar(50),
		numVisits int
)

create table inner_R_1 (
		sourceip varchar(50),
		valueOrder int
);
create table inner_R_2 (
		destip varchar(50),
		valueOrder int
);
create table inner_R_3 (
		day varchar(50),
		valueOrder int
);


-- create indices on the R_i
--create index inner_R_1_sourceip ON inner_R_1 (sourceip);
--create index inner_R_2_destip ON inner_R_2 (destip);
--create index inner_R_3_day ON inner_R_3 (day);


create table inner_B_1 (
		sourceip varchar(50)
);
create table inner_B_2 (
		destip varchar(50)
);
create table inner_B_3 (
		day varchar(50)
);

create table B_tilde_1 (
		sourceip varchar(50)
);
create table B_tilde_2 (
		destip varchar(50)
);
create table B_tilde_3 (
		day varchar(50)
);


create table current_inner_B_i_summary (
		numObs int,
		dimIndex int
)

create table inner_B_optimal (
	varValue varchar(50),
	M_B_a_n int,
	m_inner_b int,
	b_cardinality int
)

create table inner_B_optimal_temp (
	varValue varchar(50),
	M_B_a_n int,
	m_inner_b int,
	b_cardinality int,
	iterCount int
)

create table inner_B_optimal_groupedByValue (
	varValue varchar(50),
	numVisits int,
	m_inner_b int,
	b_cardinality int
)

create table D_i (
	currentIndex int,
	ID int,
	varValue varchar(50),
	m_inner_b float,
	cumSumMass float,
	b_cardinality float,
	primary key (ID)
)

create table D_i_summary (
	varValue varchar(50),
	m_inner_b float,
	valueOrder int,
	cumSumMass float,
	updatedBDim float,
	p_prime float
)

create table maximalGeomMass_D_i (
	maxPPrime float,
	argMaxOrder int
)

create table condTestValues (
	counter float,
	numDenseBlocks float,
	currentRMass float,
	M_inner_B float,
	b_cardinality float,
	b_1_cardinality float,
	b_2_cardinality float,
	b_3_cardinality float,
	p_tilde float,
	inner_r float,
	r_tilde float,
	termCondition float,
	testCounter float,
	optimalDimension int,
	nonOptimal_cardinality_1 float,
	nonOptimal_cardinality_2 float,
	currentMaxPPrime float,
	dummyCol int,
	D_i_count int;
)


create table interimCond (
	interimCond float
)

create table interimCond2 (
	interimCond float
)

create table interimCond3 (
	interimCond float
)

create table interimCond4 (
	interimCond float
)


create table interimCond5 (
	interimCond float,
	dimIndex int
)

-- results from a two-block experiment on 10% darpa sample
 -- (30 minutes)
create table twoBlockResults (
		sourceip varchar(50),
		destip varchar(50),
		day varchar(50),
		numVisits int,
		blockIndex int
)

-----------------
---- B. Declare functions
-----------------



create or replace function getMaxOrder() returns int as $$  
begin
	return (select max(valueOrder) from D_i_summary);
end;
$$ language plpgsql;


-- calculate the current mass of a table:
create or replace function getTotalCurrentRelationMass() returns float as $$  
begin
	return (select sum(numVisits) from currentR where markedTuple = 0);
end;
$$ language plpgsql;

-- calculate inner-inner loop terminating condition:
	-- if 0, then terminate while loop

create or replace function getTerminatingCondition() returns float   as $$
begin
	return (select max(a.numObs) from (
		select count(*) as numObs from inner_B_1 union all
		select count(*) as numObs from inner_B_2 union all
		select count(*) as numObs from inner_B_3
		) as a
		);
end;
$$ language plpgsql;

create or replace function getTotalInnerBlockMass() returns float as $$  
begin
	return (select sum(numVisits) from inner_B);
end;
$$ language plpgsql;
create or replace function select_dimension() returns int as $$  
begin
	return (  
		select max(dimIndex) from current_inner_B_i_summary AS A INNER JOIN
			(select max(numObs) as maxNumObs from current_inner_B_i_summary) as B
		ON A.numObs = B.maxNumObs
		);
end;
$$ language plpgsql;

create or replace function getMaxGeomAvgMass() returns float as $$  
begin
	return (select max(maxPPrime) from maximalGeomMass_D_i);
end;
$$ language plpgsql;
create or replace function getArgMaxOrder() returns float as $$  
begin
	return (select max(argMaxOrder) from maximalGeomMass_D_i);
end;
$$ language plpgsql;

create or replace function getInnerB1Dim() returns int as $$   begin return (select count(*) from inner_B_1); 
	end;
	$$ language plpgsql;
create or replace function getInnerB2Dim() returns int as $$   begin return (select count(*) from inner_B_2); 
	end;
	$$ language plpgsql;
	create or replace function getInnerB3Dim() returns int as $$   begin return (select count(*) from inner_B_3); 
	end;
	$$ language plpgsql;
-- todo: take 1/n power
create or replace function getGeomAverage(mass_b float, inner_b1_dim float, inner_b2_dim float, inner_b3_dim float) returns float as $$  
	begin
	if (inner_b1_dim = 0 or inner_b2_dim = 0 or inner_b3_dim = 0) then
		return -1;
	else
		return (mass_b * (1 / POWER((inner_b1_dim * inner_b2_dim * inner_b3_dim),(1/3)) ));
	end if;
	end;
	$$ language plpgsql;


-- because this was originally developed in MySql (which has user-defined variables; whereas psql doesn't),
	-- ... I utilize helper methods to get/store global variables
	create or replace function getcounter() returns float as $$ begin return ( select max(										counter) from condTestValues); end; $$ language plpgsql;              
	create or replace function getnumDenseBlocks() returns float as $$ begin return ( select max(					numDenseBlocks) from condTestValues); end; $$ language plpgsql; 
	create or replace function getcurrentRMass() returns float as $$ begin return ( select max(					currentRMass) from condTestValues); end; $$ language plpgsql; 
	create or replace function getM_inner_B() returns float as $$ begin return ( select max(					M_inner_B) from condTestValues); end; $$ language plpgsql; 
		create or replace function getb_cardinality() returns float as $$ begin return ( select max(					b_cardinality) from condTestValues); end; $$ language plpgsql; 
	create or replace function getb_1_cardinality() returns float as $$ begin return ( select max(					b_1_cardinality) from condTestValues); end; $$ language plpgsql; 
	create or replace function getb_2_cardinality() returns float as $$ begin return ( select max(					b_2_cardinality) from condTestValues); end; $$ language plpgsql; 
	create or replace function getb_3_cardinality() returns float as $$ begin return ( select max(					b_3_cardinality) from condTestValues); end; $$ language plpgsql; 
	create or replace function getp_tilde() returns float as $$ begin return ( select max(					p_tilde) from condTestValues); end; $$ language plpgsql; 
	create or replace function getinner_r() returns float as $$ begin return ( select max(					inner_r) from condTestValues); end; $$ language plpgsql; 
	create or replace function getr_tilde() returns float as $$ begin return ( select max(					r_tilde) from condTestValues); end; $$ language plpgsql; 
	create or replace function gettermCondition() returns float as $$ begin return ( select max(					termCondition) from condTestValues); end; $$ language plpgsql; 
	create or replace function gettestCounter() returns float as $$ begin return ( select max(					testCounter) from condTestValues); end; $$ language plpgsql; 
	create or replace function getoptimalDimension() returns float as $$ begin return ( select max(optimalDimension) from condTestValues); end; $$ language plpgsql;
	create or replace function getnonOptimal_cardinality_1() returns float as $$ begin return ( select max(					nonOptimal_cardinality_1) from condTestValues); end; $$ language plpgsql; 
	create or replace function getnonOptimal_cardinality_2() returns float as $$ begin return ( select max(					nonOptimal_cardinality_2) from condTestValues); end; $$ language plpgsql; 
	create or replace function getcurrentMaxPPrime() returns float as $$ begin return ( select max(					currentMaxPPrime) from condTestValues); end; $$ language plpgsql; 
	create or replace function getD_i_count() returns float as $$ begin return ( select max(					D_i_count) from condTestValues); end; $$ language plpgsql; 




-----------------
---- C. Declare Algorithm
-----------------




create or replace function findBlocks(in numBlocks int) returns void as $$
begin
-- instantiate currentR
truncate condTestValues; insert into condTestValues (dummyCol) select 1;
truncate currentR; truncate results; truncate R_orig; truncate interimCond; truncate interimCond2; truncate interimCond3; truncate interimCond4; truncate interimCond5;
	truncate inner_B_optimal_temp;
insert into currentR select *, 0 as markedTuple from darparaw; -- darpaRaw contains the darpa csv data, as generated in the commented section at the beginning of the file
insert into R_orig select * from darparaw;

update condTestValues set counter = 1 where dummyCol = 1; -- set @counter = 1;
update condTestValues set numDenseBlocks = numBlocks where dummyCol = 1;  --set @numDenseBlocks = 2;
while (getcounter() <= getnumDenseBlocks()) loop
	insert into interimCond select count(*) from currentR where markedTuple = 0;
	-- i. calculate the current rel mass
	update condTestValues set currentRMass = getTotalCurrentRelationMass() where dummyCol = 1;  -- set @currentRMass = getTotalCurrentRelationMass();
-- select @currentRMass;
		-- ii. get the block (inputs: currentR, current_R_i, @currentRMass):
	truncate inner_B;
	insert into inner_B select sourceip, destip, day, numVisits from currentR  where markedTuple = 0;
	update condTestValues set M_inner_B = getcurrentRMass() where dummyCol = 1;  -- set @M_inner_B = @currentRMass;
	truncate inner_R_1; truncate inner_R_2; truncate inner_R_3;
	insert into inner_R_1 (sourceip) select distinct sourceip from currentR where markedTuple = 0;
	insert into inner_R_2 (destip) select distinct destip from currentR where markedTuple = 0;
	insert into inner_R_3 (day) select distinct day from currentR where markedTuple = 0;

 	truncate inner_B_1; truncate inner_B_2; truncate inner_B_3;
	insert into inner_B_1 select sourceip from inner_R_1;
	insert into inner_B_2 select destip from inner_R_2;
	insert into inner_B_3 select day from inner_R_3;
	update condTestValues set b_1_cardinality = getInnerB1Dim() where dummyCol = 1;  -- set @b_1_cardinality = getInnerB1Dim();
	update condTestValues set b_2_cardinality = getInnerB2Dim() where dummyCol = 1;  -- set @b_1_cardinality = getInnerB1Dim();
	update condTestValues set b_3_cardinality = getInnerB3Dim() where dummyCol = 1;  -- set @b_1_cardinality = getInnerB1Dim();


	update condTestValues set p_tilde = getGeomAverage(getM_inner_B(), getb_1_cardinality(), getb_2_cardinality(), getb_3_cardinality() ) where dummyCol = 1;
	update condTestValues set inner_r = 0 where dummyCol = 1; -- set @inner_r = 0;
	update condTestValues set r_tilde = 1 where dummyCol = 1; -- set @r_tilde = 1;
	update condTestValues set termCondition = getTerminatingCondition() where dummyCol = 1; -- set @termCondition = getTerminatingCondition();


	update condTestValues set testCounter = 0 where dummyCol = 1; -- set @testCounter = 0;

	while (gettermCondition() > 0) loop
	-- while (@testCounter <= 30 and @termCondition > 0) loop
		-- a. get max cardinality and produce the auxiliary table inner_B_optimal
		update condTestValues set M_inner_B = getTotalInnerBlockMass() where dummyCol = 1; -- set @M_inner_B = getTotalInnerBlockMass();
		truncate current_inner_B_i_summary;
		insert into current_inner_B_i_summary
			select count(*) as numObs, 1 as dimIndex from inner_B_1 group by dimIndex union all
			select count(*) as numObs, 2 as dimIndex from inner_B_2 group by dimIndex union all
			select count(*) as numObs, 3 as dimIndex from inner_B_3 group by dimIndex;
		update condTestValues set optimalDimension = select_dimension() where dummyCol = 1; -- set @optimalDimension = select_dimension();
		-- select @optimalDimension;
		insert into interimCond3 select getoptimalDimension();
		truncate inner_B_optimal;
		if (getoptimalDimension() = 1) then
			update condTestValues set b_cardinality = getInnerB1Dim() where dummyCol = 1;
			update condTestValues set nonOptimal_cardinality_1 = getInnerB2Dim() where dummyCol = 1;
			update condTestValues set nonOptimal_cardinality_2 = getInnerB3Dim() where dummyCol = 1;
			insert into inner_B_optimal
				select c.sourceip as varValue, c.M_B_a_n as M_B_a_n, getM_inner_B() as m_inner_b, getb_cardinality() as b_cardinality from (
				select a.sourceip, sum(a.numVisits) as M_B_a_n from inner_B as a inner join inner_B_1 as b
				on a.sourceip = b.sourceip
				group by a.sourceip
				) as c;
		elsif (getoptimalDimension() = 2) then
			update condTestValues set b_cardinality = getInnerB2Dim() where dummyCol = 1;
			update condTestValues set nonOptimal_cardinality_1 = getInnerB1Dim() where dummyCol = 1;
			update condTestValues set nonOptimal_cardinality_2 = getInnerB3Dim() where dummyCol = 1;
			insert into inner_B_optimal (varValue, M_B_a_n, m_inner_b, b_cardinality)
				select c.destip as varValue, c.M_B_a_n as M_B_a_n, getM_inner_B() as m_inner_b, getb_cardinality() as b_cardinality from (
				select a.destip, sum(a.numVisits) as M_B_a_n from inner_B as a inner join inner_B_2 as b
				on a.destip = b.destip
				group by a.destip
				) as c;
		else
			update condTestValues set b_cardinality = getInnerB3Dim() where dummyCol = 1;
			update condTestValues set nonOptimal_cardinality_1 = getInnerB1Dim() where dummyCol = 1;
			update condTestValues set nonOptimal_cardinality_2 = getInnerB2Dim() where dummyCol = 1;
			insert into inner_B_optimal
				select c.day as varValue, c.M_B_a_n as M_B_a_n, getM_inner_B() as m_inner_b, getb_cardinality() as b_cardinality from (
				select a.day, sum(a.numVisits) as M_B_a_n from inner_B as a inner join inner_B_3 as b
				on a.day = b.day
				group by a.day
				) as c;
	  end if;

		-- b. calculate D_i and the max geom avg (p_prime) via D_i_summary
		-- select * from inner_B_optimal;
 		truncate D_i;
		insert into D_i
		select a.currentIndex::int,  currentIndex::int + getinner_r() as ID, a.varValue, a.m_inner_b, sum(a.M_B_a_n) over (order by currentIndex asc) as cumSumMass, a.b_cardinality from
		(select varValue, M_B_a_n, m_inner_b, b_cardinality, row_number() over(order by M_B_a_n asc) as currentIndex
			from inner_B_optimal where M_B_a_n <= m_inner_b / b_cardinality order by M_B_a_n asc) as a;

		--select (@t := @t + 1) as currentIndex, c.* from (select (@inner_r + (@r := @r + 1)) AS ID, b.* from
		--	( select a.varValue, a.m_inner_b, (@s := @s + a.M_B_a_n) AS cumSumMass, b_cardinality AS b_cardinality from (
		-- select varValue, M_B_a_n, m_inner_b, b_cardinality from inner_B_optimal where M_B_a_n <= m_inner_b / b_cardinality
		-- ) as a, (SELECT @s := 0) dm order by a.M_B_a_n) as b, (SELECT @r := 0) dm1) as c, (SELECT @t := 0) dm3 ;


		-- select * from inner_B_optimal;
		-- select count(*) from inner_B_1;
		-- select * from inner_B where destip = "152.163.210.053";

		truncate D_i_summary;
		insert into D_i_summary
		select varValue, m_inner_b, ID as valueOrder, cumSumMass, b_cardinality - ID as updatedBDim,
			getGeomAverage(m_inner_b - cumSumMass, b_cardinality - currentIndex, getnonOptimal_cardinality_1(), getnonOptimal_cardinality_2()) as p_prime
		from D_i;


		-- now, get the max

		truncate maximalGeomMass_D_i;
		insert into maximalGeomMass_D_i
			select p_prime as maxPPrime, valueOrder as argMaxOrder from D_i_summary where p_prime = (select max(p_prime) from D_i_summary);
		-- compare vs current p_tilde
		update condTestValues set currentMaxPPrime = getMaxGeomAvgMass() where dummyCol = 1;

		-- select @currentMaxPPrime, @p_tilde;

		if (getcurrentMaxPPrime() > getp_tilde()) then
			update condTestValues set p_tilde = getcurrentMaxPPrime();
			update condTestValues set r_tilde = getArgMaxOrder() where dummyCol = 1;
				insert into interimCond4 select getr_tilde();
		end if;

			-- 	select @r_tilde, @p_tilde;


		-- update the ranking variable
		update condTestValues set D_i_count = (select count(*) from D_i) where dummyCol = 1;
		if (getD_i_count() > 0) then
			update condTestValues set inner_r = getMaxOrder() where dummyCol = 1;
				insert into interimCond5 select getinner_r(), getoptimalDimension();
		end if;

					-- 	select @r_tilde, @inner_r;
		-- select max(valueOrder) from D_i_summary;

		-- update the ordering in inner_R_i:
		if (getoptimalDimension() = 1) then
			update inner_R_1 set valueOrder = D_i.ID from D_i
				where inner_R_1.sourceip = D_i.varValue;
		elsif (getoptimalDimension() = 2) then
			update inner_R_2  set valueOrder = D_i.ID from D_i
				where inner_R_2.destip = D_i.varValue;
		else
			update inner_R_3 set  valueOrder = D_i.ID from D_i
				where inner_R_3.day = D_i.varValue;
		end if;

		-- finally, update B by removing all optimal variable values in D_i
		truncate inner_B_temp;
		insert into inner_B_temp select * from inner_B;
		-- select count(*) from inner_B_1;
		truncate inner_B;
		if (getoptimalDimension() = 1) then
			insert into inner_B
			select a.* from inner_B_temp as a left outer join D_i as b on a.sourceip = b.varValue
			where b.ID is null ;
			truncate inner_B_1;
			insert into inner_B_1 (sourceip) select distinct sourceip from inner_B;
		elsif (getoptimalDimension() = 2) then
			insert into inner_B
			select a.* from inner_B_temp as a left outer join D_i as b on a.destip = b.varValue
			where b.ID is null ;
			-- also update B_2:
			truncate inner_B_2;
			insert into inner_B_2 (destip) select distinct destip from inner_B;
		else
			insert into inner_B
			select a.* from inner_B_temp as a left outer join D_i as b on a.day = b.varValue
			where b.ID is null ;
			truncate inner_B_3;
			insert into inner_B_3 (day) select distinct day from inner_B;
		end if;
	update condTestValues set termCondition = getTerminatingCondition() where dummyCol = 1;
	update condTestValues set testCounter = gettestCounter() + 1 where dummyCol = 1;
	insert into interimCond2 select gettermCondition();
	end loop; -- end while loop


	-- finally, output the B_n_tildes
	truncate B_tilde_1; truncate B_tilde_2; truncate B_tilde_3;
	insert into B_tilde_1 select sourceip from inner_R_1 where valueOrder >= getr_tilde();
	insert into B_tilde_2 select destip from inner_R_2 where valueOrder >= getr_tilde();
	insert into B_tilde_3 select day from inner_R_3 where valueOrder >= getr_tilde();

	-- iii. restrict the current R:
	--truncate currentR2;
	--insert into currentR2 select * from currentR;
	--truncate currentR;
	--insert into currentR
	--	select distinct f.sourceip, f.destip, f.day, f.numVisits from
	--(select a.* from currentR2 as a left outer join B_tilde_1 as b on a.sourceip = b.sourceip where b.sourceip is null union all
	--select a.* from currentR2 as a left outer join B_tilde_2 as c on a.destip = c.destip where c.destip is null union all
	--select a.* from currentR2 as a left outer join B_tilde_3 as d on a.day = d.day where d.day is null) as f;
		insert into currentR_log
	select count(*) as rowCount from currentR where markedTuple = 0;
			
	update currentR set markedTuple = 1  where (sourceip in (select sourceip from B_tilde_1)) and (destip in (select destip from B_tilde_2)) and (day in (select day from B_tilde_3));
	--update currentR set markedTuple = 1 from B_tilde_2 where currentR.destip = B_tilde_2.destip;
	--update currentR set markedTuple = 1 from B_tilde_3 where currentR.day = B_tilde_3.day;

		insert into currentR_log
	select count(*) as rowCount from currentR where markedTuple = 0;
	-- iv. calculate B_ori:
	truncate B_ori;
	insert into B_ori
	select f1.* from
		(select f2.* from
			(select a.* from R_orig as a inner join
				B_tilde_3 as d on a.day = d.day) as f2 inner join
					B_tilde_2 as c on f2.destip = c.destip) as f1 inner join
						B_tilde_1 as b on f1.sourceip = b.sourceip ;

	-- v. store the results:
	insert into results
	select *, getcounter() as blockIndex from B_ori;


	-- select * from results;
	update condTestValues set counter = getcounter() + 1 where dummyCol = 1;
end loop;
end; -- end procedure definition
$$ language plpgsql;



-----------------
---- D. Call the algorithm
-----------------


truncate results; truncate currentR_log;
select findBlocks(3); -- populates the "results" table, which is displayed below:
select blockIndex, count(*) from results group by blockIndex;
select * from currentR_log;




