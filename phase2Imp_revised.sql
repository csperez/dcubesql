
create function getTerminatingCondition() returns float
begin
	return (select max(a.numObs) from (
		select count(*) as numObs from inner_B_1 union all
		select count(*) as numObs from inner_B_2 union all
		select count(*) as numObs from inner_B_3
		) as a
		);
end;

create function getTotalInnerBlockMass() returns float
begin
	return (select sum(numVisits) from inner_B);
end;

create function select_dimension() returns int
begin
	return (  
		select max(dimIndex) from current_inner_B_i_summary AS A INNER JOIN
			(select max(numObs) as maxNumObs from current_inner_B_i_summary) as B
		ON A.numObs = B.maxNumObs
		);
end;

create function getMaxGeomAvgMass() returns float
begin
	return (select max(maxPPrime) from maximalGeomMass_D_i);
end;
create function getArgMaxOrder() returns float
begin
	return (select max(argMaxOrder) from maximalGeomMass_D_i);
end;


create function getInnerB1Dim() returns int begin return (select count(*) from inner_B_1); 
	end;
create function getInnerB2Dim() returns int begin return (select count(*) from inner_B_2); 
	end;
	create function getInnerB3Dim() returns int begin return (select count(*) from inner_B_3); 
	end;
-- todo: take 1/n power
create function getGeomAverage(mass_b float, inner_b1_dim float, inner_b2_dim float, inner_b3_dim float) returns float
	begin
	if (inner_b1_dim = 0 or inner_b2_dim = 0 or inner_b3_dim = 0) then
		return -1;
	else
		return (mass_b * (1 / POWER((inner_b1_dim * inner_b2_dim * inner_b3_dim),(1/3)) ));
	end if;
	end;
/*
Instantiate data structures
*/

-- Test run: populate data structures with temp data
truncate currentR;
insert into currentR select * from darpaRaw;
insert into inner_R_1 (sourceip) select distinct sourceip, null from currentR;
insert into inner_R_2 (destip) select distinct destip from currentR;
insert into inner_R_3 (day) select distinct date(day) from currentR;
set @currentRMass = getTotalCurrentRelationMass();


-- Test run: get a single block
truncate inner_B;
insert into inner_B select * from currentR;
set @M_inner_B = @currentRMass
truncate inner_B_1; truncate inner_B_2; truncate inner_B_3;
insert into inner_B_1 select sourceip from inner_R_1;
insert into inner_B_2 select destip from inner_R_2;
insert into inner_B_3 select day from inner_R_3;
set @b_1_cardinality = getInnerB1Dim();
set @b_2_cardinality = getInnerB2Dim();
set @b_3_cardinality = getInnerB3Dim();

set @p_tilde = getGeomAverage(@M_inner_B, @b_1_cardinality, @b_2_cardinality, @b_3_cardinality )
set @inner_r = 1;
set @r_tilde = 1;
set @termCondition = getTerminatingCondition();

while @termCondition > 0 do
	-- a. get max cardinality and produce the auxiliary table inner_B_optimal
	truncate current_inner_B_i_summary;
	insert into current_inner_B_i_summary
		select count(*) as numObs, 1 as dimIndex from inner_B_1 group by dimIndex union all
		select count(*) as numObs, 2 as dimIndex from inner_B_2 group by dimIndex union all
		select count(*) as numObs, 3 as dimIndex from inner_B_3 group by dimIndex;
	set @optimalDimension = select_dimension();
	truncate inner_B_optimal;
	if (@optimalDimension = 1)
		set @b_cardinality = getInnerB1Dim();
		set @nonOptimal_cardinality_1 = getInnerB2Dim();
		set @nonOptimal_cardinality_2 = getInnerB3Dim();
		insert into inner_B_optimal select c.sourceip as varValue, c.M_B_a_n as M_B_a_n, @M_inner_B as m_inner_b, @b_cardinality as b_cardinality from (
			select a.sourceip, sum(numVisits) as M_B_a_n from inner_B as a inner join inner_B_1 as b
			on a.sourceip = b.sourceip
			group by a.sourceip
			) as c;
	else if (@optimalDimension = 2)
		set @b_cardinality = getInnerB2Dim();
		set @nonOptimal_cardinality_1 = getInnerB1Dim();
		set @nonOptimal_cardinality_2 = getInnerB3Dim();
		insert into inner_B_optimal select c.destip as varValue, c.M_B_a_n as M_B_a_n, @M_inner_B as m_inner_b, @b_cardinality as b_cardinality from (
			select a.destip, sum(numVisits) as M_B_a_n from inner_B as a inner join inner_B_2 as b
			on a.destip = b.destip
			group by a.destip
			) as c;	
	else
		set @b_cardinality = getInnerB3Dim();
		set @nonOptimal_cardinality_1 = getInnerB1Dim();
		set @nonOptimal_cardinality_2 = getInnerB2Dim();
		insert into inner_B_optimal select c.day as varValue, c.M_B_a_n as M_B_a_n, @M_inner_B as m_inner_b, @b_cardinality as b_cardinality from (
			select a.day, sum(numVisits) as M_B_a_n from inner_B as a inner join inner_B_3 as b
			on a.day = b.day
			group by a.day
			) as c;	
	-- b. calculate D_i and the max geom avg (p_prime) via D_i_summary
	truncate D_i;
	insert into D_i
	select (@r := @r + 1) AS ID, b.* from ( select a.varValue, a.m_inner_b, (@s := @s + a.M_B_a_n) AS cumSumMass, b_cardinality AS b_cardinality from (
	select varValue, M_B_a_n, m_inner_b, b_cardinality from inner_B_optimal where M_B_a_n <= m_inner_b / b_cardinality
	) as a, (SELECT @s := 0) dm order by a.M_B_a_n) as b, (SELECT @r := 0) dm1;

	truncate D_i_summary;
	insert into D_i_summary
	select varValue, m_inner_b, ID as valueOrder, cumSumMass, b_cardinality - ID as updatedBDim, getGeomAverage(m_inner_b - cumSumMass, b_cardinality - ID, @nonOptimal_cardinality_1, @nonOptimal_cardinality_2) as p_prime
	from D_i;
	-- now, get the max 
	insert into maximalGeomMass_D_i
		select p_prime as maxPPrime, valueOrder as argMaxOrder from D_i_summary where p_prime = (select max(p_prime) from D_i_summary);  
	-- compare vs current p_tilde
	set @currentMaxPPrime = getMaxGeomAvgMass();
	if (@currentMaxPPrime > @p_tilde)
		set @p_tilde = @currentMaxPPrime;
		set @r_tilde = getArgMaxOrder();
	-- update the ordering in inner_R_i:
	if (@optimalDimension = 1)
		update inner_R_1, D_i set inner_R_1.valueOrder = D_i.ID
			where inner_R_1.sourceip = D_i.varValue
	else if (@optimalDimension = 2)
	update inner_R_2, D_i set inner_R_2.valueOrder = D_i.ID
			where inner_R_2.destip = D_i.varValue
	else
	update inner_R_3, D_i set inner_R_3.valueOrder = D_i.ID
			where inner_R_3.day = D_i.varValue
	
	-- finally, update B by removing all optimal variable values in D_i
	truncate inner_B_temp;
	insert into inner_B_temp select * from inner_B;
	truncate inner_B;
	if (@optimalDimension = 1)
		insert into inner_B
		select a.* from inner_B_temp as a left outer join D_i as b on a.sourceip = b.varValue
		where isnull(b.ID) ;
	else if (@optimalDimension = 2)
		insert into inner_B
		select a.* from inner_B_temp as a left outer join D_i as b on a.destip = b.varValue
		where isnull(b.ID) ;
	else
		insert into inner_B
		select a.* from inner_B_temp as a left outer join D_i as b on a.day = b.varValue
		where isnull(b.ID) ;
set @termCondition = getTerminatingCondition();
end while; -- end while loop

-- finally, output the B_n_tildes
insert into B_tilde_1 select sourceip from inner_R_1 where valueOrder >= @r_tilde; 
insert into B_tilde_2 select destip from inner_R_2 where valueOrder >= @r_tilde;
insert into B_tilde_3 select day from inner_R_3 where valueOrder >= @r_tilde;

-- restrict the current R:
truncate currentR2;
insert into currentR2 select * from currentR;
truncate currentR;
insert into currentR
	select distinct f.sourceip, f.destip, f.day, f.numVisits from
(select a.* from currentR2 as a left outer join B_tilde_1 as b on a.sourceip = b.sourceip where isnull(b.sourceip) union all
select a.* from currentR2 as a left outer join B_tilde_2 as c on a.destip = c.destip where isnull(c.destip) union all
select a.* from currentR2 as a left outer join B_tilde_3 as d on a.day = d.day where isnull(d.day)) as f

-- calculate B_ori:
truncate B_ori;
insert into B_ori
select f1.* from 
	(select f2.* from 
		(select a.* from currentR2 as a inner join 
			B_tilde_3 as d on a.day = d.day) as f2 inner join 
				B_tilde_2 as c on f2.destip = c.destip) as f1 inner join
					B_tilde_1 as b on f1.sourceip = b.sourceip ; 


-- store the results:
insert into results
select *, @counter as blockIndex from B_ori



-- Note: to create a block B from the individual unique value B_i's,
	-- ... do a sequential join, keeping only those rows which match all B_i's:
	-- e.g. R = ((R \join B_1) \join B_2) \join B_3 


/*
start loop
*/



DROP PROCEDURE IF EXISTS darpaData.findBlocks;

create procedure findBlocks()
begin
-- instantiate currentR
truncate currentR; truncate results; truncate R_orig;
insert into currentR select * from darpaRaw;
insert into R_orig select * from darpaRaw;

set @counter = 1;
set @numDenseBlocks = 1;
while @counter <= @numDenseBlocks do
	-- i. calculate the current rel mass
	set @currentRMass = getTotalCurrentRelationMass();

		-- ii. get the block (inputs: currentR, current_R_i, @currentRMass):
	truncate inner_B;
	insert into inner_B select * from currentR;
	set @M_inner_B = @currentRMass;
	truncate inner_R_1; truncate inner_R_2; truncate inner_R_3;
	insert into inner_R_1 (sourceip) select distinct sourceip from currentR;
	insert into inner_R_2 (destip) select distinct destip from currentR;
	insert into inner_R_3 (day) select distinct date(day) from currentR;

 	truncate inner_B_1; truncate inner_B_2; truncate inner_B_3;
	insert into inner_B_1 select sourceip from inner_R_1;
	insert into inner_B_2 select destip from inner_R_2;
	insert into inner_B_3 select day from inner_R_3;
	set @b_1_cardinality = getInnerB1Dim();
	set @b_2_cardinality = getInnerB2Dim();
	set @b_3_cardinality = getInnerB3Dim();


	set @p_tilde = getGeomAverage(@M_inner_B, @b_1_cardinality, @b_2_cardinality, @b_3_cardinality );
	set @inner_r = 1;
	set @r_tilde = 1;
	set @termCondition = getTerminatingCondition();

	-- test
	select @termCondition;

	set @testCounter = 0;

	-- while (@termCondition > 0 and @testCounter <= 2) do
	while (@testCounter <= 5 and @termCondition > 0) do
		-- a. get max cardinality and produce the auxiliary table inner_B_optimal
		set @M_inner_B = getTotalInnerBlockMass();
		truncate current_inner_B_i_summary;
		insert into current_inner_B_i_summary
			select count(*) as numObs, 1 as dimIndex from inner_B_1 group by dimIndex union all
			select count(*) as numObs, 2 as dimIndex from inner_B_2 group by dimIndex union all
			select count(*) as numObs, 3 as dimIndex from inner_B_3 group by dimIndex;
		set @optimalDimension = select_dimension();
		truncate inner_B_optimal;
		if (@optimalDimension = 1) then
			set @b_cardinality = getInnerB1Dim();
			set @nonOptimal_cardinality_1 = getInnerB2Dim();
			set @nonOptimal_cardinality_2 = getInnerB3Dim();
			insert into inner_B_optimal
				select c.sourceip as varValue, c.M_B_a_n as M_B_a_n, @M_inner_B as m_inner_b, @b_cardinality as b_cardinality from (
				select a.sourceip, sum(a.numVisits) as M_B_a_n from inner_B as a inner join inner_B_1 as b
				on a.sourceip = b.sourceip
				group by a.sourceip
				) as c;
		elseif (@optimalDimension = 2) then
			set @b_cardinality = getInnerB2Dim();
			set @nonOptimal_cardinality_1 = getInnerB1Dim();
			set @nonOptimal_cardinality_2 = getInnerB3Dim();
			insert into inner_B_optimal (varValue, M_B_a_n, m_inner_b, b_cardinality)
				select c.destip as varValue, c.M_B_a_n as M_B_a_n, @M_inner_B as m_inner_b, @b_cardinality as b_cardinality from (
				select a.destip, sum(a.numVisits) as M_B_a_n from inner_B as a inner join inner_B_2 as b
				on a.destip = b.destip
				group by a.destip
				) as c;
		else
			set @b_cardinality = getInnerB3Dim();
			set @nonOptimal_cardinality_1 = getInnerB1Dim();
			set @nonOptimal_cardinality_2 = getInnerB2Dim();
			insert into inner_B_optimal
				select c.day as varValue, c.M_B_a_n as M_B_a_n, @M_inner_B as m_inner_b, @b_cardinality as b_cardinality from (
				select a.day, sum(a.numVisits) as M_B_a_n from inner_B as a inner join inner_B_3 as b
				on a.day = b.day
				group by a.day
				) as c;
	  end if;

	  insert into inner_B_optimal_temp
	  	select *, @counter as iterCount from inner_B_optimal;
		-- b. calculate D_i and the max geom avg (p_prime) via D_i_summary
		-- select * from inner_B_optimal;
 		truncate D_i;
		insert into D_i
		select (@r := @r + 1) AS ID, b.* from ( select a.varValue, a.m_inner_b, (@s := @s + a.M_B_a_n) AS cumSumMass, b_cardinality AS b_cardinality from (
		select varValue, M_B_a_n, m_inner_b, b_cardinality from inner_B_optimal where M_B_a_n <= m_inner_b / b_cardinality
		) as a, (SELECT @s := 0) dm order by a.M_B_a_n) as b, (SELECT @r := 0) dm1;


		-- select * from inner_B_optimal;
		-- select * from D_i;
		-- select * from inner_B where destip = "152.163.210.053";


		truncate D_i_summary;
		insert into D_i_summary
		select varValue, m_inner_b, ID as valueOrder, cumSumMass, b_cardinality - ID as updatedBDim, getGeomAverage(m_inner_b - cumSumMass, b_cardinality - ID, @nonOptimal_cardinality_1, @nonOptimal_cardinality_2) as p_prime
		from D_i;

		-- now, get the max

		truncate maximalGeomMass_D_i;
		insert into maximalGeomMass_D_i
			select p_prime as maxPPrime, valueOrder as argMaxOrder from D_i_summary where p_prime = (select max(p_prime) from D_i_summary);
		-- compare vs current p_tilde
		set @currentMaxPPrime = getMaxGeomAvgMass();

		if (@currentMaxPPrime > @p_tilde) then
			set @p_tilde = @currentMaxPPrime;
			set @r_tilde = getArgMaxOrder();
		end if;

		-- update the ordering in inner_R_i:
		if (@optimalDimension = 1) then
			update inner_R_1, D_i set inner_R_1.valueOrder = D_i.ID
				where inner_R_1.sourceip = D_i.varValue;
		elseif (@optimalDimension = 2) then
			update inner_R_2, D_i set inner_R_2.valueOrder = D_i.ID
				where inner_R_2.destip = D_i.varValue;
		else
			update inner_R_3, D_i set inner_R_3.valueOrder = D_i.ID
				where inner_R_3.day = D_i.varValue;
		end if;

		-- finally, update B by removing all optimal variable values in D_i
		truncate inner_B_temp;
		insert into inner_B_temp select * from inner_B;
		truncate inner_B;
		if (@optimalDimension = 1) then
			insert into inner_B
			select a.* from inner_B_temp as a left outer join D_i as b on a.sourceip = b.varValue
			where isnull(b.ID) ;
			truncate inner_B_1;
			insert into inner_B_1 (sourceip) select distinct sourceip from inner_B;
		elseif (@optimalDimension = 2) then
			insert into inner_B
			select a.* from inner_B_temp as a left outer join D_i as b on a.destip = b.varValue
			where isnull(b.ID) ;
			-- also update B_2:
			truncate inner_B_2;
			insert into inner_B_2 (destip) select distinct destip from inner_B;
		else
			insert into inner_B
			select a.* from inner_B_temp as a left outer join D_i as b on a.day = b.varValue
			where isnull(b.ID) ;
			truncate inner_B_3;
			insert into inner_B_3 (day) select distinct date(day) from inner_B;
		end if;
	set @termCondition = getTerminatingCondition();
	set @testCounter = @testCounter + 1;
	insert into interimCond select @termCondition;
	end while; -- end while loop

	-- finally, output the B_n_tildes
	insert into B_tilde_1 select sourceip from inner_R_1 where valueOrder >= @r_tilde;
	insert into B_tilde_2 select destip from inner_R_2 where valueOrder >= @r_tilde;
	insert into B_tilde_3 select day from inner_R_3 where valueOrder >= @r_tilde;

	-- iii. restrict the current R:
	truncate currentR2;
	insert into currentR2 select * from currentR;
	truncate currentR;
	insert into currentR
		select distinct f.sourceip, f.destip, f.day, f.numVisits from
	(select a.* from currentR2 as a left outer join B_tilde_1 as b on a.sourceip = b.sourceip where isnull(b.sourceip) union all
	select a.* from currentR2 as a left outer join B_tilde_2 as c on a.destip = c.destip where isnull(c.destip) union all
	select a.* from currentR2 as a left outer join B_tilde_3 as d on a.day = d.day where isnull(d.day)) as f;

	-- iv. calculate B_ori:
	truncate B_ori;
	insert into B_ori
	select f1.* from
		(select f2.* from
			(select a.* from R_orig as a inner join
				B_tilde_3 as d on a.day = d.day) as f2 inner join
					B_tilde_2 as c on f2.destip = c.destip) as f1 inner join
						B_tilde_1 as b on f1.sourceip = b.sourceip ;

	-- v. store the results:
	insert into results
	select *, @counter as blockIndex from B_ori;


	select * from results;
	set @counter = @counter + 1;
end while;
end; -- end procedure definition



SELECT @@global.secure_file_priv;

select 5 INTO OUTFILE '/Users/Chris/Downloads/sqlTest1.csv'
FIELDS TERMINATED BY ','
ENCLOSED BY '"'
LINES TERMINATED BY '\n';

select * from interimCond;
truncate results;

call findBlocks();

flush tables;
show open tables;

drop table inner_B;
drop table inner_B_2;
drop table inner_B_optimal;

create table interimCond (
	interimCond float
)

create table interimCond2 (
	interimCond float
)

create table interimCond3 (
	interimCond float
)

create table interimCond4 (
	interimCond float
)


create table interimCond5 (
	interimCond float,
	dimIndex int
)

-- results from a two-block experiment
 -- (30 minutes)
create table twoBlockResults (
		sourceip nvarchar(50),
		destip nvarchar(50),
		day nvarchar(50),
		numVisits int,
		blockIndex int
)