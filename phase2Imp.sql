
/*
Notes

-- correct function declaration:
create function my_concat (p_str1 longtext, p_str2 longtext) returns longtext
    begin
      return concat(p_str1,p_str2);
   end ;
-- correct function call:
select my_concat('foo','bar');

-- example:
create function getTotalCurrentRelationMass() returns float
begin
	return (select sum(numVisits) from darpaRaw);
end;

select getTotalCurrentRelationMass();

--------------
--- Sources
--------------
http://www.sqlteam.com/article/intro-to-user-defined-functions-updated
http://stackoverflow.com/questions/6768198/sqlserver-scalar-function-returning-result-of-select
http://dba.stackexchange.com/questions/47266/overwrite-table-with-data-from-another-table-sql
http://www.wiseowl.co.uk/blog/s348/queries-in-loops.htm

*/



/*
1. Declare the helper functions and temporary tables 
*/

-- calculate inner-inner loop terminating condition:
	-- if 0, then terminate while loop
create function getTerminatingCondition() returns float
begin
	return (select max(numObs) from (
		select count(*) as numObs from inner_B_1 union all
		select count(*) as numObs from inner_B_2 union all
		select count(*) as numObs from inner_B_3 union all
		)
		);
end;

-- calculate M_B_a_n
create function getMassBlockValue(a varchar) returns float
begin
	return (
		);
end;

create function getMaxOrder() returns int
begin
	return (select max(valueOrder) from D_i_summary);
end;

-- get dim with max cardinality
-- get argmax as well
-- might have to be multistatement
create function select_dimension() returns float
begin
	return (select max(numObs) from (
		select count(*) as numObs, 1 as dimIndex from inner_B_1 groupBy dimIndex union all
		select count(*) as numObs, 2 as dimIndex from inner_B_2 groupBy dimIndex union all
		select count(*) as numObs, 3 as dimIndex from inner_B_3 groupBy dimIndex union all
		)
		);
end;


-- calculate the current mass of a table:
create function getTotalCurrentRelationMass() returns float
begin
	return (select sum(numVisits) from currentR);
end;

-- calculate the geo degree of a block in the current relation
create function calculateGeoAverageMass()
	returns float
as
begin

return
end

-- get a single dense block
create function find_single_block(currentRelMass float, denseBlockIndex int) 
	returns @singleDenseBlock table (
				sourceip nvarchar(50),
		destip nvarchar(50),
		day nvarchar(50),
		numVisits int,
		blockIndex int
)
begin
insert into @singleDenseBlock
select *, denseBlockIndex as blockIndex from currentR limit 10;
-- i. initialize block, unique block values, max measure, and orders
-- ii. while loop
end;

create table interimCond (
	interimCond float
)

-- create temporary tables containing both the current relation and the current unique values of the tensor variables
create table currentR (
		sourceip nvarchar(50),
		destip nvarchar(50),
		day nvarchar(50),
		numVisits int
)

create table R_orig (
		sourceip nvarchar(50),
		destip nvarchar(50),
		day nvarchar(50),
		numVisits int
)

create table currentR2 (
		sourceip nvarchar(50),
		destip nvarchar(50),
		day nvarchar(50),
		numVisits int
)
create table currentSingleBlock (
		sourceip nvarchar(50),
		destip nvarchar(50),
		day nvarchar(50),
		numVisits int,
		blockIndex int
)
create table currentSourceUniques (
		sourceip nvarchar(50)
)
create table currentDestUniques (
		destip nvarchar(50)
)
create table currentDayUniques (
		day nvarchar(50)
)

-- inner loop data tables:
create table B_ori (
		sourceip nvarchar(50),
		destip nvarchar(50),
		day nvarchar(50),
		numVisits int
)
create table results (
		sourceip nvarchar(50),
		destip nvarchar(50),
		day nvarchar(50),
		numVisits int,
		blockIndex int
)
create table results_temp (
		sourceip varchar(50),
		destip varchar(50),
		day varchar(50),
		numVisits int,
		blockIndex int
)

create table inner_B (
		sourceip nvarchar(50),
		destip nvarchar(50),
		day nvarchar(50),
		numVisits int
)
create table inner_B_temp (
		sourceip nvarchar(50),
		destip nvarchar(50),
		day nvarchar(50),
		numVisits int
)

create table inner_R_1 (
		sourceip nvarchar(50),
		valueOrder int
);
create table inner_R_2 (
		destip nvarchar(50),
		valueOrder int
);
create table inner_R_3 (
		day nvarchar(50),
		valueOrder int
);

create table inner_B_1 (
		sourceip nvarchar(50)
);
create table inner_B_2 (
		destip nvarchar(50)
);
create table inner_B_3 (
		day nvarchar(50)
);

create table B_tilde_1 (
		sourceip nvarchar(50)
);
create table B_tilde_2 (
		destip nvarchar(50)
);
create table B_tilde_3 (
		day nvarchar(50)
);

-- also import the |B_i| and M_B terms
create table inner_B_1_groupedByVal (
		sourceip nvarchar(50),
		numVisits float,
		indexMeasure float,
		totalBlockMeasure float
)
create table inner_B_2_groupedByVal (
		destip nvarchar(50),
		numVisits float,
		indexMeasure float,
		totalBlockMeasure float
)
create table inner_B_3_groupedByVal (
		day nvarchar(50),
		numVisits float,
		indexMeasure float,
		totalBlockMeasure float
)

create table inner_3Level_Block (
	varValue nvarchar(50)
)

create table current_inner_B_i_summary (
		numObs int,
		dimIndex int
)

create table inner_B_optimal (
	varValue nvarchar(50),
	M_B_a_n int,
	m_inner_b int,
	b_cardinality int
)

create table inner_B_optimal_temp (
	varValue nvarchar(50),
	M_B_a_n int,
	m_inner_b int,
	b_cardinality int,
	iterCount int
)

create table inner_B_optimal_groupedByValue (
	varValue nvarchar(50),
	numVisits int,
	m_inner_b int,
	b_cardinality int
)

create table D_i (
	currentIndex int,
	ID int NOT NULL AUTO_INCREMENT,
	varValue nvarchar(50),
	m_inner_b float,
	cumSumMass float,
	b_cardinality float,
	primary key (ID)
)

create table D_i_summary (
	varValue nvarchar(50),
	m_inner_b float,
	valueOrder int,
	cumSumMass float,
	updatedBDim float,
	p_prime float
)

create table maximalGeomMass_D_i (
	maxPPrime float,
	argMaxOrder int
)


-- inner 3-level loop strategy:
	-- i. in D_i define the cumulative sum of M_b_a_i, with M_B in another column
		-- use optimal and "other two" |B_n|'s to calculate geom
		-- update order() in D_i with @r, update @r
		-- with inner_p_prime > inner_p_tilde:
			-- update the variables

-- now that D_i has orders, update inner_B
-- also, update the input R_n with the orders from D_i
-- With the final r_tilde, 


 -- insert into darpaRaw(sourceip, destip, connectTime, numVisits)
 --  select  C1, C2, STR_TO_DATE(C3, '%m/%d/%Y-%H:%i') as connectTime, count(*) as numVisits from darpa_with_labels_tenPercSample
-- 	 group by C1, C2, connectTime;
insert into currentR select * from darpaRaw;

select distinct sourceip into currentSourceUniques from darpaRaw;
select distinct destip into currentDestUniques from darpaRaw;
select distinct day into currentDayUniques from darpaRaw;

-- create the final results table:
create finalResults table(
		sourceip nvarchar(50),
		destip nvarchar(50),
		day nvarchar(50),
		numVisits int,
		blockIndex int -- stores the index of the anomalous block found
)


/*
2. Do the block-finding loop
*/

-- actual find_single_block procedure
--input: currentR, currentRMass
-- a. initilialize the structures, and calculate p_tilde = geom(B,R)

insert into inner_B select * from currentR;
set @M_inner_B = @curentRMass
insert into inner_B_1 select * from currentR_1;
insert into inner_B_2 select * from currentR_2;
insert into inner_B_3 select * from currentR_3;
set @p_tilde = calcGeom()
set @inner_r = 1;
set @inner_r_tilde = 1;


-- b. structure while loop as:
	-- set count_termCondi	tion = select max(varCounts) from (B_1 union B_2 union B_3)
-- c. compute the M_b_a_n structures
-- d. get max cardinality dimension
-- e. produce D_i structure and sort according to M_b_a_i
-- f. inner loop:
	-- reduce B_i and M_B
	-- calculate temporary p_prime
	-- set order(a,i) <- r and r <- r+1
	-- compare p_prime p_tilde
		-- update p_tilde and r_tilde
-- g. reduce B (Note: not variable-by-variable as in the overarching loop)

-- h. reconstruct B_n_tilde using r_tilde

-- temp procedure

truncate currentR;
insert into currentR select * from darpaRaw;
truncate currentR2;
truncate currentSingleBlock;

DROP PROCEDURE IF EXISTS darpaDB.findBlocks;

create procedure findBlocks()
begin
set @counter = 1;
set @numDenseBlocks = 5;
while @counter <= @numDenseBlocks do
	-- i. calculate the current rel mass
	set @currentRMass = getTotalCurrentRelationMass();



	-- ii. get the block:
	truncate currentSingleBlock;
	insert into currentSingleBlock
	select distinct *, @counter as blockIndex from currentR limit 10;

		-- let's say B_1 is chosen:
		-- (actually, prob better to define it as a "generic" column)



	-- iii. store the restriction of the block and the currentR into currentR2
	insert into currentR2
		select A.sourceip, A.destip, A.day, A.numVisits from currentR as A left outer join currentSingleBlock as B on
		a.sourceip = b.sourceip and a.destip = b.destip and a.day = b.day
		where isnull(b.sourceip); -- get those rows that don't match
	truncate currentR;
	insert into currentR select * from currentR2; -- update R
	truncate currentR2; -- reset the temporary table
	set @counter = @counter + 1;
end while;
end;

select count(*) from darpaRaw;



call findBlocks();



/*
create function findBlocks(@kValue smallint)
BEGIN
-- 1. set parameters
DECLARE @counter INT
DECLARE @numDenseBlocks INT
declare currentRMass int
SET @counter = 1
SET @numDenseBlocks = @kValue
set currentRMass = getTotalCurrentRelationMass()
-- 2. declare intermediate data

-- 3. run the dense block loop
while @counter <= @numDenseBlocks do
-- find the single dense block
insert into currentSingleBlock
select *, denseBlockIndex as blockIndex from currentR limit 10;
-- i. compute the current mass of R, and find the unique values of the tensor variables
-- ii. find a single block 
-- iii. reset R
-- iv. calculate and store the block's representation in R_ori (since the block was found in the current restricted version of R_ori)
-- v. add the updated block to the cumulative results


SET @Counter += 1
END -- end k dense block loop

return
end;
*/


/*
4. Output the blocks found
*/



Current datagrip input

-- select C1 from darpa_with_label;

/*
create table darpaRaw(
		sourceip TEXT,
		destip TEXT,
		connectTime datetime,
		numVisits int

)
*/

/*
create function find_single_block(currentRelMass float, denseBlockIndex int)
	returns int
begin
insert into currentSingleBlock
select *, denseBlockIndex as blockIndex from currentR limit 10;
end;

SET @counter = 1;
SET @numDenseBlocks = 5;
*/

create procedure findBlocks()
begin
while @counter <= @numDenseBlocks do
	select @counter;
	set @counter = @counter + 1;
end while;
end;

call doWhile();



create procedure findBlocks()
begin
while @counter <= @numDenseBlocks do
	insert into currentSingleBlock
	select *, @counter as blockIndex from currentR limit 10;
	set @counter = @counter + 1;
end while;
end;

select @counter;

truncate currentR;
insert into currentR select * from darpaRaw;
truncate currentR2;
truncate currentSingleBlock;

DROP PROCEDURE IF EXISTS darpaDB.findBlocks;

create procedure findBlocks()
begin
set @counter = 1;
set @numDenseBlocks = 5;
while @counter <= @numDenseBlocks do
	select @counter;
	-- i. get the block:
	truncate currentSingleBlock;
	insert into currentSingleBlock
	select distinct *, @counter as blockIndex from currentR limit 10;
	-- ii. store the restriction of the block and the currentR into currentR2
	insert into currentR2
		select A.sourceip, A.destip, A.day, A.numVisits from currentR as A left outer join currentSingleBlock as B on
		a.sourceip = b.sourceip and a.destip = b.destip and a.day = b.day
		where isnull(b.sourceip); -- get those rows that don't match
	truncate currentR;
	insert into currentR select * from currentR2; -- update R
	truncate currentR2; -- reset the temporary table
	set @counter = @counter + 1;
end while;
end;

select count(*) from currentR2;



call findBlocks();

 -- select  C3, STR_TO_DATE(C3, '%m/%d/%Y-%H:%i') as connectTime from darpa_with_labels_tenPercSample;
















